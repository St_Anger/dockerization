# app-front

## Clone front project and rename cloned folder to "app-front"
```
cd projects
git clone git@gitlab.com:St_Anger/Koroglu.git app-front
```

### Copy docker-compose.example.yml and name it docker-compose.yml in the same folder

### Create node_modules directory in projects/app-front directory
```
sudo mkdir node_modules
sudo chmod -R 777 node_modules
```

### Create docker network:
```
docker network create --gateway 172.1.0.1 --subnet 172.1.0.0/24 gs_network
```

### In docker-wrapper project root (goods-and-services) execute:
```
docker-compose up -d app-front
```

### Check that app-front container created and running
```
docker-compose ps -a
```

### Open browser on localhost:8081


